import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-popover-etiqueta',
  templateUrl: './popover-etiqueta.page.html',
  styleUrls: ['./popover-etiqueta.page.scss'],
})
export class PopoverEtiquetaPage implements OnInit {
  public ids;
  public etiqueta;
  constructor(private popover:PopoverController,
    private alertController: AlertController,
    public api: ApiSchoolnoteService) { }

  ngOnInit() {
  }

  guardarEtiqueta(){
    for (let id in this.ids){
      let params = {
        "idMultimedia": this.ids[id].toString(),
        "etiqueta": this.etiqueta.toString()
      };
      let res = this.api.post("agregarEtiqueta", params).subscribe(response => {
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.ClosePopover();
        } else if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
    }
  }

  ClosePopover()
  {
    this.popover.dismiss();
  }

  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }

}
