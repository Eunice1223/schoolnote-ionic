import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PopoverEtiquetaPage } from './popover-etiqueta.page';

describe('PopoverEtiquetaPage', () => {
  let component: PopoverEtiquetaPage;
  let fixture: ComponentFixture<PopoverEtiquetaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoverEtiquetaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PopoverEtiquetaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
