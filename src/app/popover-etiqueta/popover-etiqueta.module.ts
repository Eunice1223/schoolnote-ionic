import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopoverEtiquetaPageRoutingModule } from './popover-etiqueta-routing.module';

import { PopoverEtiquetaPage } from './popover-etiqueta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopoverEtiquetaPageRoutingModule
  ],
  declarations: [PopoverEtiquetaPage]
})
export class PopoverEtiquetaPageModule {}
