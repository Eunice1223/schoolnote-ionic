import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';


const ID_KEY = "id_key";
@Injectable({
  providedIn: 'root'
})

export class AuthService {

  authState =  new BehaviorSubject(false);

  constructor(private storage: Storage, private platform: Platform) {
    this.platform.ready().then(() => {
      this.checkToken();
    });
   }

  login(id){
    console.log(id);
    return this.storage.set(ID_KEY, id).then(res => {
      this.authState.next(true);
    });
  }
  logout(){
    return this.storage.remove(ID_KEY).then(res => {
      this.authState.next(false);
    });
  }
  isAuthenticated(){
    return this.authState.value;
  }
  getID(){
    return this.storage.get(ID_KEY);
  }
  checkToken(){
    return this.storage.get(ID_KEY).then(res => {
      if(res){
        this.authState.next(true);
      }
    });
  }
}
