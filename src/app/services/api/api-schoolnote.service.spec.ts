import { TestBed } from '@angular/core/testing';

import { ApiSchoolnoteService } from './api-schoolnote.service';

describe('ApiSchoolnoteService', () => {
  let service: ApiSchoolnoteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiSchoolnoteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
