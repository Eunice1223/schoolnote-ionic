import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

//const urlBase = 'http://ec2-18-218-6-103.us-east-2.compute.amazonaws.com:1711/api/'

@Injectable({
  providedIn: 'root'
})
export class ApiSchoolnoteService {
  urlBase = 'http://ec2-18-218-6-103.us-east-2.compute.amazonaws.com:1711/api/';
  httpOptionsMultipart = {
    headers: new HttpHeaders({
      'enctype': 'multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA'
    }),
    'responseType': 'text' as 'json'
  };
  httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'text/plain',
      'Content-Type': 'application/json'      
    }),
    'responseType': 'text' as 'json'
  };

  constructor(
    private myHttp: HttpClient
  ){
  }
  post(url, params){
    return this.myHttp.post(this.urlBase+url, params, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandle)
    );
  }
  get(url){
    return this.myHttp.get(this.urlBase+url, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandle)
    );
  }
  postMultipart(url, formData: FormData){
    return this.myHttp
        .post(this.urlBase+url,formData,this.httpOptionsMultipart)
        .pipe(
          retry(1),
          catchError(this.errorHandle)
        );;
}
  errorHandle(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Client-side error
      errorMessage = error.error.message;
    } else {
      // Server-side error
      errorMessage = `Error Código: ${error.status}\nMsg: ${error.message}`;
    }
    console.log(`Error: `, errorMessage);
    return throwError(errorMessage);
  }
}
