import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const PROFILE_KEY = "profile_key"
@Injectable({
  providedIn: 'root'
})
export class ProfileInfoService {

  constructor(private storage: Storage) { }

  saveProfileInfo(json){
    this.storage.set(PROFILE_KEY, json).then(res => {
      console.log(res)
    });
  }
  getProfileInfo(){
    return this.storage.get(PROFILE_KEY);
  }
  updateProfileInfo(json){
    return this.storage.set(PROFILE_KEY, json);
  }
}
