import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'noticias',
        loadChildren: () => import('../pages/noticias/noticias.module').then(m => m.NoticiasPageModule)
      },
      {
        path: 'archivos',
        loadChildren: () => import('../pages/archivos/archivos.module').then(m => m.ArchivosPageModule)
      },
      {
        path: 'calendario',
        loadChildren: () => import('../pages/calendario/calendario.module').then(m => m.CalendarioPageModule)
      },
      {
        path: 'pendientes',
        loadChildren: () => import('../pages/pendientes/pendientes.module').then(m => m.PendientesPageModule)
      },
      {
        path: 'menu',
        loadChildren: () => import('../pages/menu/menu.module').then(m => m.MenuPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/calendario',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/calendario',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
