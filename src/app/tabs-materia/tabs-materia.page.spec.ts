import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabsMateriaPage } from './tabs-materia.page';

describe('TabsMateriaPage', () => {
  let component: TabsMateriaPage;
  let fixture: ComponentFixture<TabsMateriaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsMateriaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabsMateriaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
