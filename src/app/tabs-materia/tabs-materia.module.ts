import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabsMateriaPageRoutingModule } from './tabs-materia-routing.module';

import { TabsMateriaPage } from './tabs-materia.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsMateriaPageRoutingModule
  ],
  declarations: [TabsMateriaPage]
})
export class TabsMateriaPageModule {}
