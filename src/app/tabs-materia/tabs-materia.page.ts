import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from '../services/api/api-schoolnote.service';
import { LoaderService } from '../services/api/loader.service';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-tabs-materia',
  templateUrl: './tabs-materia.page.html',
  styleUrls: ['./tabs-materia.page.scss'],
})
export class TabsMateriaPage implements OnInit {

  private idUsuario;
  private materia;
  private isAdmin = false;
  private hasGroup = false;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private authService: AuthService) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.materia= JSON.parse(this.router.getCurrentNavigation().extras.state.materia);
        console.log(this.materia);
        this.hasGroup = this.materia.idGrupo != null;
      }
    });
    }
  ngOnInit() {
    this.authService.getID().then(id =>{
      this.idUsuario = id
      this.isAdmin = this.materia.admin == this.idUsuario;
    })
    this.hasGroup = this.materia.idGrupo != null;
  }
 navigateTo(route){
  let navigationExtras: NavigationExtras = { state:{materia: JSON.stringify(this.materia)}};
  this.router.navigate(['tabs-materia/'+route], navigationExtras);
 }
 presentPopOver(){
  let navigationExtras: NavigationExtras = { state:{materia: JSON.stringify(this.materia), toGroup: true}};
  this.router.navigate(['/agregar'], navigationExtras);
 }
}
