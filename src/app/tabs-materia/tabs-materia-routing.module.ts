import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsMateriaPage } from './tabs-materia.page';

const routes: Routes = [
  {
    path: '',
    component: TabsMateriaPage,
    children: [
      {
        path: 'archivos',
        loadChildren: () => import('../pages/materia-archivos/materia-archivos.module').then(m => m.MateriaArchivosPageModule)
      },
      {
        path: 'chat',
        loadChildren: () => import('../pages/materia-chat/materia-chat.module').then(m => m.MateriaChatPageModule)
      },
      {
        path: 'pendientes',
        loadChildren: () => import('../pages/pendientes/pendientes.module').then(m => m.PendientesPageModule)
      },
      {
        path: 'configuracion',
        loadChildren: () => import('../pages/materia-configuracion/materia-configuracion.module').then(m => m.MateriaConfiguracionPageModule)
      },
      {
        path: '',
        redirectTo: 'pendientes',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsMateriaPageRoutingModule {}
