import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth/auth.service';
import { Router } from '@angular/router';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { ApiSchoolnoteService } from './services/api/api-schoolnote.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private router: Router,
    private api: ApiSchoolnoteService,
    private fcm: FCM
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleBlackTranslucent();
      this.splashScreen.hide();
    });
    this.authService.authState.subscribe(state => {
      console.log("Auth state: " + state)
      if(state){
        this.router.navigate(["tabs"]);
        this.initFirebase();
      }
      else
        this.router.navigate(["login"]);

    });
  }
  initFirebase(){
    if (this.platform.is('android') || this.platform.is('ios')) {
      this.fcm.getToken().then(token => {
        console.log(token);
        this.sendToken(token);
      });
      this.fcm.subscribeToTopic('General');
      this.fcm.onNotification().subscribe(data => {
        console.log(data);
        if (data.wasTapped) {
          console.log('Received in background');
        } else {
          console.log('Received in foreground');
        }
      });
      this.fcm.onTokenRefresh().subscribe((token: string) => {
        console.log(`Got a new token ${token}`);
        this.sendToken(token);
      });
    }
  }
  sendToken(token){
    this.authService.getID().then(id => {
      let params = {
        "token": token.toString(),
        "idUsuario": id.toString()
      };
      this.api.post("agregarToken", params).subscribe(response => {
        let res = JSON.parse(response.toString());
        if(res['message'] != undefined)
          console.log("Se envió el token");
          console.log(res['message']);
       }, error => {
        console.log(error);
      });
    });
  }
}
