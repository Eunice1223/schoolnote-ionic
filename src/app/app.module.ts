import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { PopovercomponentPageModule } from "./pages/popovercomponent/popovercomponent.module";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { IonicStorageModule} from "@ionic/storage";
import { registerLocaleData } from '@angular/common';
import localeEsMx from '@angular/common/locales/es-MX';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { PreviewAnyFile } from '@ionic-native/preview-any-file/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Camera} from '@ionic-native/camera/ngx';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { PipesModule } from './pipes/pipes.module';
 
registerLocaleData(localeEsMx);

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule, 
    HttpClientModule, 
    IonicStorageModule.forRoot(),
    PopovercomponentPageModule,
     PipesModule],
    
  providers: [
    StatusBar,
    SplashScreen,
    HttpClient,
    FCM,
    FileChooser,
    FilePath,
    File,
    FileTransfer,
    AndroidPermissions,
    PreviewAnyFile,
    InAppBrowser,
    Camera,
    DatePicker,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
