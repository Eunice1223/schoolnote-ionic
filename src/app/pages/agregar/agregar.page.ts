import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { FileEntry, File, IFile, Entry, DirectoryEntry} from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileChooser, FileChooserOptions } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AlertController, NavController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Plugins, Capacitor, FilesystemDirectory } from '@capacitor/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { PhotoService } from 'src/app/services/photo.service';
import { DatePipe } from '@angular/common';
const {Filesystem} = Plugins;

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {

  private materias = new Array();
  private materiasDia = new Array();
  private idMateriaSeleccionada;
  private materia;
  private idGrupo;
  private idUsuario;
  private filesPath;
  private fileType;
  private filesName;
  private toGroup = false;
  private photoUri;

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute, 
    private authService: AuthService, 
    private fileChooser: FileChooser,
    private filePath: FilePath,
    private file: File,
    private navCtrl: NavController,
    private transfer: FileTransfer,
    public photoService: PhotoService, 
    private androidPermissions: AndroidPermissions) { 
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          try{
          this.toGroup = this.router.getCurrentNavigation().extras.state.toGroup;
          this.materia = JSON.parse(this.router.getCurrentNavigation().extras.state.materia);
          } catch (error){
            console.log(error);
          }
        }
      });
    }

  ngOnInit() {
    this.authService.getID().then(idUsuario =>{
      this.idUsuario = idUsuario;
    });
    this.getMaterias();
  }

  async getMaterias(){
  await this.loader.presentLoading();
  let days = [7,1,2,3,4,5,6];
  let date = new Date();
  let pipe = new DatePipe('es-MX');
    this.authService.getID().then(idUsuario =>{
      let params = { 
        "idUsuario": idUsuario.toString(),
        "dia": days[date.getDay()],
        "hora": pipe.transform(date, 'hh:mm:ss')
      };
      console.log(params);
      let res = this.api.post("obtenerMateriasHorario", params).subscribe(response => {
        this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.materias = res['data'];
          console.log(this.materias);
          this.cargarMaterias();
        } else if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });

  }

  cargarMaterias(){
    let listaMateria =  this.materias;
    console.log(listaMateria);

    while (this.materiasDia.length > 0)
       this.materiasDia.pop();

       listaMateria.forEach(materiaItem => {
        this.materiasDia.push(materiaItem);
  });
  }

  navigateTo(route){
    switch (route){
      case '/files':
        this.checkPermissions(false);
        break;
      case '/images':
        this.checkPermissions(false);
        break;
      case '/camera':
          this.checkPermissions(true);
          break;
      default:
        var navigationExtras: NavigationExtras;
        if(this.toGroup){
          navigationExtras= { state:{idMateria: this.materia.idMateria, idGrupo: this.materia.idGrupo}};
        } else {
          navigationExtras= { state:{idMateria: this.idMateriaSeleccionada}};
        }
        
      this.router.navigate([route], navigationExtras);
        break;
    }
  }
  checkPermissionsCamera(){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      result => {
        if (result.hasPermission) {
          this.checkPermissionsWrite();
        } else {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA).then(result => {
            if (result.hasPermission) {
            this.checkPermissionsWrite();
            }
          });
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
    );
  
  }
  checkPermissionsWrite(){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => {
        if (result.hasPermission) {
          this.takePhoto();
        } else {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(result => {
            if (result.hasPermission) {
            this.takePhoto();
            }
          });
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    );
  
  }
  checkPermissions(camera){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
      result => {
        if (result.hasPermission) {
          if(camera)
            this.checkPermissionsCamera();
          else
            this.openFileSelector();
        } else {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => {
            if (result.hasPermission) {
              if(camera)
                this.checkPermissionsCamera();
              else
                this.openFileSelector();
            }
          });
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    );

  }
  async takePhoto(){
    this.photoUri = await this.photoService.takePhoto();
    console.log(this.photoUri );
    await this.loader.presentLoading();
    console.log("fetching blob");
    const base64Response = await fetch(this.photoUri);
    console.log("got blob");
    let blob = await base64Response.blob();
    console.log("blob created");
    let filename = "IMG_"+Date.now()+".jpg";
    let formData = new FormData();
      formData.append("idUsuario", this.idUsuario.toString());
      formData.append("nombre", filename);
      if (this.idMateriaSeleccionada != undefined){
        formData.append("idMateria", this.idMateriaSeleccionada.toString());
      }
      if (this.toGroup){
        formData.append("idGrupo", this.materia.idGrupo.toString());
        formData.append("idMateria", this.materia.idMateria.toString());
      }
      formData.append('archivo', blob, filename);
      console.log(formData.toString());
      this.api.postMultipart("agregarMultimedia", formData).subscribe(
        res =>{
          this.loader.dismissLoader();
          if(res['data'] != undefined){
            this.showError("Se ha guardado el archivo");
            this.navCtrl.pop(); 
          } else if(res['message'] != undefined)
            this.showError(res['message']);
        }, 
        error =>{
          this.loader.dismissLoader();
          console.log(error);
          this.showError("Error al conectarse al servidor");
        });
  }
  openFileSelector(){
    this.fileChooser.open().then(uri =>
      {
        console.log(uri);
        this.filePath.resolveNativePath(uri).then(filePath =>
          {
            this.filesPath = filePath;
            this.file.resolveLocalFilesystemUrl(filePath).then(fileInfo =>
              {
                console.log("file info " + fileInfo);
                let files = fileInfo as FileEntry;
                files.file(success =>
                  {
                    this.fileType   = success.type;
                    this.filesName  = success.name;
                    const capacitorFile = Capacitor.convertFileSrc(filePath);
                    let name = capacitorFile.substring(capacitorFile.lastIndexOf('/')+1, capacitorFile.length);
                    this.transferFileToServer(uri, name);
                  });
              },err =>
              {
                console.log("error obteniendo archivo " + err);
                throw err;
              });
          },err =>
          {
            console.log("error obteniendo archivo " + err);
            throw err;
          });
      },err =>
      {
        console.log(err);
        throw err;
      });
  }
  async transferFileToServer(uri, name){
    console.log(name);
    await this.loader.presentLoading();
    var parameters = {
      "idUsuario": this.idUsuario.toString(),
      "nombre": name.toString()
    };
    if (this.idMateriaSeleccionada != undefined){
      parameters["idMateria"]=this.idMateriaSeleccionada.toString();
    }
    if (this.toGroup){
      parameters["idGrupo"]=this.materia.idGrupo.toString();
      parameters["idMateria"]=this.materia.idMateria.toString();
    }
    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {
      fileKey: 'archivo',
      fileName: name+this.fileType,
      httpMethod: "POST",
      mimeType: this.fileType,
      headers: this.api.httpOptionsMultipart,
      params:parameters
   };
   console.log(parameters.toString());
   fileTransfer.upload(uri, this.api.urlBase+"agregarMultimedia", options)
    .then((data) => {
      console.log(data);
      this.loader.dismissLoader();
      this.showError("Se ha guardado el archivo");
      this.navCtrl.pop();
    }, (err) => {
      console.log(err);
      this.loader.dismissLoader();
      this.showError("Ocurrió un error al guardar el archivo");
    });
  }
  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }
 cambioMateria($event){
  console.log($event.target.value);
  this.idMateriaSeleccionada = $event.target.value;
  console.log(this.idMateriaSeleccionada);
}

}
