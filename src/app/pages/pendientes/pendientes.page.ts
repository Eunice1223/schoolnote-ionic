import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';
//import { reverse } from 'dns';
//import { ConsoleReporter } from 'jasmine';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-pendientes',
  templateUrl: './pendientes.page.html',
  styleUrls: ['./pendientes.page.scss'],
})
export class PendientesPage implements OnInit {

  private pendientes = new Array();
  private pendientesPass = new Array();
  private pendientesSemana = new Array();
  private pendientesPasados = new Array();
  private materia;
  private idPendiente;
  private idUsuario;
  fechaActual: Date = new Date();
  hoy: Date = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate());
  hoyFecha= '';

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute, 
    private authService: AuthService) { 
      this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.materia= JSON.parse(this.router.getCurrentNavigation().extras.state.materia);
        console.log(this.materia);
      }
    });}

  ngOnInit() {
    console.log(this.fechaActual);
    //console.log(this.fechaActual.datePipe.transform(date,"yyyy-MM-dd"));
    this.hoyFecha=this.hoy.toISOString();
    console.log('hoyFecha:'+this.hoyFecha);
   /*  console.log(this.d);
    this.d = this.d.split('T')[0];
    console.log(this.d); */
    this.getPendientes(null);
  }

 

  openDetail(id, idMateria, titulo, fecha, descripcion, fechaRaw){
    console.log("Entrando en openDetail");
    let navigationExtras: NavigationExtras = { state:{idPendiente: id, idMateria: idMateria, titulo:titulo, fechaEntrega: fecha, descripcion:descripcion, fechaRaw:fechaRaw}};
    this.router.navigate(['editar-pendiente'], navigationExtras);
   }
   

  async getPendientes(refresher){
    await this.loader.presentLoading();
    this.authService.getID().then(idUsuario =>{
    let params = {
      "idUsuario": idUsuario.toString(),
    };
    if (this.materia != undefined){
      params["idMateria"] = this.materia.idMateria.toString();
    }
    let res = this.api.post("obtenerPendientes", params).subscribe(response => {
      
      this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      console.log('res=',res);
      if(res['data'] != undefined){
        this.filterPendientes(res['data']);
        this.pendientesPass= res;
        console.log('res=',res);
      } else if(res['message'] != undefined){
        this.showError(res['message']);
      }
     }, error => {
      this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
    if(refresher != null)
      refresher.target.complete();
  }
  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }
 filterPendientes(listaPendientes){
  this.pendientes = [];
  this.pendientesSemana = [];
  this.pendientesPasados = [];
  console.log(listaPendientes);
  //listaPendientes.reverse();        //Acomodar la vista para mostrar primero pendientes recientes. EDIT: Ya no se necesita, se actualizó la api
  let pipe = new DatePipe('es-MX');
  let weekEndDate = this.getWeekEndDate();
  let finSemana= weekEndDate.toISOString();
  listaPendientes.forEach(pendiente => {
      
      console.log('FECHA RAW: '+pendiente.fechaRaw);
      let sqlDate = pendiente.fechaEntrega.split("T", 1)[0];
      sqlDate = sqlDate + "T10:00:00.000z"
      pendiente.fechaRaw= sqlDate;
      let hoySplit = this.hoyFecha.split("T", 1)[0];
      console.log('hoySplit'+hoySplit);
      let date = new Date(sqlDate);
      //date.setHours(11 ,  11, 11);
      let fechaHoy = new Date(hoySplit);
      console.log('fechaHoy'+fechaHoy);
      console.log('pendiente.fechaEntrega='+pendiente.fechaEntrega);
      pendiente.fechaEntrega = pipe.transform(date, 'fullDate');
      console.log('pendiente.fechaEntrega(Transform)='+pendiente.fechaEntrega);
      if(sqlDate <= finSemana &&  sqlDate >= hoySplit){
        this.pendientesSemana.push(pendiente);
      }else if(sqlDate > hoySplit){
        this.pendientes.push(pendiente);
      } else {
        this.pendientesPasados.push(pendiente);
        //console.log('PENDIENTE PASADO AGREGADO');
        //console.log(this.pendientesPasados);
      }
      /* this.pendientesSemana.reverse();
      this.pendientes.reverse();
      this.pendientesPasados.reverse(); */
      
  });
 }
 getWeekEndDate() {
  let now = new Date();
  let dayOfWeek = now.getDay(); //0-6
  let numDay = now.getDate();
  let end = new Date(now); //copy
  end.setDate(numDay + (7 - dayOfWeek));
  end.setHours(0, 0, 0, 0);
  return end;
}

eliminarPendiente(idPendiente){
  console.log("Entrando en eliminarPendiente");
  this.authService.getID().then(idUsuario =>{
  let params = {
    "idUsuario": idUsuario.toString(),
    "idPendiente":idPendiente.toString(),    
  };
  if (this.materia != undefined){
    params["idGrupo"] = this.materia.idGrupo.toString(); 
  }
  console.log(params);
  let res = this.api.post("eliminarPendiente", params).subscribe(response => {
    this.loader.dismissLoader();
    let res = JSON.parse(response.toString());
    this.ngOnInit();
    //this.router.navigate(['/tabs/pendientes']);
    /* if(res['data'] != undefined){
      this.filterPendientes(res['data']);
      this.pendientesPass= res;
    } else if(res['message'] != undefined){
      this.showError(res['message']);
    } */
   }, error => {
    this.loader.dismissLoader();
    console.log(error);
    this.showError("Error al conectarse al servidor");
  });
  });
  
}


}
