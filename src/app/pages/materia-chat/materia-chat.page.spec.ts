import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MateriaChatPage } from './materia-chat.page';

describe('MateriaChatPage', () => {
  let component: MateriaChatPage;
  let fixture: ComponentFixture<MateriaChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MateriaChatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MateriaChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
