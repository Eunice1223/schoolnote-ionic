import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MateriaChatPage } from './materia-chat.page';

const routes: Routes = [
  {
    path: '',
    component: MateriaChatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MateriaChatPageRoutingModule {}
