import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, Platform } from '@ionic/angular';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import {Input, ViewChild} from '@angular/core';
import {IonContent} from '@ionic/angular';
import {IonInput} from '@ionic/angular';

@Component({
  selector: 'app-materia-chat',
  templateUrl: './materia-chat.page.html',
  styleUrls: ['./materia-chat.page.scss'],
})
export class MateriaChatPage implements OnInit {
 
  @ViewChild(IonContent,  {read: IonContent, static: false}) list: IonContent;
  @ViewChild(IonInput,  {read: IonInput, static: false}) message: IonInput;
  private materia;
  private idUsuario = 0;
  private mensajes = Array();
  private mensaje;
  private lastID = -1;
  
  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute, 
    private authService: AuthService,
    private fcm: FCM,
    private platform: Platform) {
      this.platform.ready()
      .then(() => {
        this.fcm.onNotification().subscribe(data => {
          if (data.wasTapped) {
            console.log("Received in background");
          } else {
            console.log("Received in foreground");
          };
          this.getChat();
        });
      });
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.materia= JSON.parse(this.router.getCurrentNavigation().extras.state.materia);
        }
      });
     }

  ngOnInit() {
    this.authService.getID().then(id =>{
      this.idUsuario = id;
    }
    );
    this.getChat();
  }
  getChat(){
    //this.loader.presentLoading();
    let params = {
      "idGrupo": this.materia.idGrupo.toString()
    };
    let res = this.api.post("obtenerMensajes", params).subscribe(response => {
      //this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.mensajes = res['data'];
        this.mensajes.reverse();
        this.scrollToBottom();
      } else if(res['message'] != undefined){
        this.showError(res['message']);
      }
     }, error => {
      //this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
  }
  scrollToBottom(){
    setTimeout(() => {
      this.list.scrollToBottom(200);
   }, 500);
  }
  sendMessage(){
    let params = {
      "idGrupo": this.materia.idGrupo.toString(),
      "mensaje": this.mensaje,
      "idUsuario": this.idUsuario.toString()
    };
    let res = this.api.post("enviarMensaje", params).subscribe(response => {
      let res = JSON.parse(response.toString());
      if(res['success'] == 1){
        this.getChat();
      } else if(res['message'] != undefined){
        this.showError(res['message']);
      }
      this.message.value = "";
      this.message.setBlur();
     }, error => {
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
  }
  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
  }
}
