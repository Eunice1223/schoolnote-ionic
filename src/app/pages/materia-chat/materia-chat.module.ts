import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MateriaChatPageRoutingModule } from './materia-chat-routing.module';

import { MateriaChatPage } from './materia-chat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MateriaChatPageRoutingModule
  ],
  declarations: [MateriaChatPage]
})
export class MateriaChatPageModule {}
