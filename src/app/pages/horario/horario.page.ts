import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-horario',
  templateUrl: './horario.page.html',
  styleUrls: ['./horario.page.scss'],
})

export class HorarioPage implements OnInit {

  private horario = new Array();
  private horarioGlobal = new Array();
  private horarioDia = new Array();
  private materias = new Array();
  private materiasDia = new Array();
  public idMateria;
  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
    this.getMaterias();
  }

  async getMaterias(){

    this.authService.getID().then(idUsuario =>{
      this.loader.presentLoading();
      let params = { 
        "idUsuario": idUsuario.toString() 
      };
      let res = this.api.post("obtenerListaMaterias", params).subscribe(response => {
        this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.materias = res['data'];
          console.log(this.materias);
          //$('#selectMaterias').value = this.materias[0].idMateria;
          this.cargarMaterias();
          this.getHorario();
        } else if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });

  }

  cargarMaterias(){
    let listaMateria =  this.materias;
    console.log(listaMateria);

    while (this.materiasDia.length > 0)
       this.materiasDia.pop();

       listaMateria.forEach(materiaItem => {
        this.materiasDia.push(materiaItem);
  });

  }
 
  async getHorario(){
    this.authService.getID().then(idUsuario =>{
      //this.loader.presentLoading();
      let params = { 
        "idUsuario": idUsuario.toString() 
      };
      let res = this.api.post("obtenerHorario", params).subscribe(response => {
        //this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.horarioGlobal = res['data'];
          console.log(this.horarioGlobal);
          this.cargarHorarioMateria(this.horarioGlobal[0].idMateria);
        } else if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });
  }

  cargarHorarioMateria(idMateriaS){
    let listaHorario =  this.horarioGlobal;
   console.log(listaHorario);

   while (this.horarioDia.length > 0)
      this.horarioDia.pop();

      while (this.horario.length > 0)
      this.horario.pop();

    listaHorario.forEach(horarioItem => {
     let idMateriaSelec = horarioItem.idMateria;
      if (horarioItem.horaInicio.split('T').length > 1){
        horarioItem.horaInicio = horarioItem.horaInicio.split("T")[1].slice(0,-8);
      }
      if (horarioItem.horaFin.split('T').length > 1){
        horarioItem.horaFin = horarioItem.horaFin.split("T")[1].slice(0,-8);
      }
     if(idMateriaSelec == this.idMateria){
       this.horarioDia.push(horarioItem);
     } else {
       this.horario.push(horarioItem);
     }
 });
    
  }

async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }

editarHorario(){
  let navigationExtras: NavigationExtras = { state:{idMateria: this.idMateria}};
  this.router.navigate(['agregar-horario'], navigationExtras);
}

cambioMateria($event){
console.log($event.target.value);
}

}
