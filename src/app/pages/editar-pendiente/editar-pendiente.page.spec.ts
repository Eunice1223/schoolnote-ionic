import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditarPendientePage } from './editar-pendiente.page';

describe('EditarPendientePage', () => {
  let component: EditarPendientePage;
  let fixture: ComponentFixture<EditarPendientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarPendientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditarPendientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
