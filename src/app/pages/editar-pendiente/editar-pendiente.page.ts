import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileInfoService } from 'src/app/services/profile/profile-info.service';

@Component({
  selector: 'app-editar-pendiente',
  templateUrl: './editar-pendiente.page.html',
  styleUrls: ['./editar-pendiente.page.scss'],
})
export class EditarPendientePage implements OnInit {

  private fecha;
  private fechaRaw;
  private idGrupo;
  private descripcion;
  private titulo;
  private recargar=true;

  private color;
  private materias = new Array();
  private materiasDia = new Array();

  pendiente: any;
  private idPendiente;

  constructor(
    private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private authService: AuthService,
    private router: Router,
    private route:ActivatedRoute,
    private profileInfo: ProfileInfoService
  ) { 

    //console.log("Prueba");
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.pendiente = this.router.getCurrentNavigation().extras.state;
        //this.idPendiente = this.router.getCurrentNavigation().extras.state.idPendiente;
        console.log("Prueba");
        console.log(this.pendiente);
        console.log(this.pendiente.idPendiente);
        console.log(this.pendiente.idMateria);
        console.log(this.pendiente.titulo);
        console.log(this.pendiente.fechaEntrega);
        console.log(this.pendiente.descripcion);
        //console.log(this.idPendiente);

        this.titulo=this.pendiente.titulo;
        this.fecha=this.pendiente.fecha;
        this.fechaRaw=this.pendiente.fechaRaw;
        this.descripcion=this.pendiente.descripcion;
      }
    });

  }

  ngOnInit() {
    console.log('FECHA RAW = '+this.fechaRaw)
  }

  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
  }

  validateForm(){
    if(this.titulo != undefined && this.fechaRaw != undefined && this.descripcion != undefined ){
        this.editarPendiente()
        

        let navigationExtras: NavigationExtras = { state:{recargar: this.recargar}};
        this.router.navigate(['/tabs/pendientes'], navigationExtras);
    } else {
      this.showError("Por favor llena todos los campos");
    }
  }

  editarPendiente(){
    this.authService.getID().then(idUsuario =>{
      //this.loader.presentLoading();
    let params = {
     
      "idMateria": this.pendiente.idMateria.toString(),
      "descripcion": this.descripcion,
      "titulo": this.titulo,
      "idPendiente":this.pendiente.idPendiente.toString(),
      "fecha": this.fechaRaw
    };
    let res = this.api.post("editarPendiente", params).subscribe(response => {
      //this.loader.dismissLoader();
      console.log(response);
      console.log(response.toString());
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        //this.nombre = "";
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      //this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });    
  }

}
