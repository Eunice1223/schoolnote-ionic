import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditarPendientePageRoutingModule } from './editar-pendiente-routing.module';

import { EditarPendientePage } from './editar-pendiente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditarPendientePageRoutingModule
  ],
  declarations: [EditarPendientePage]
})
export class EditarPendientePageModule {}
