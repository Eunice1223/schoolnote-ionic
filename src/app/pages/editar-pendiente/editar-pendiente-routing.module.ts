import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditarPendientePage } from './editar-pendiente.page';

const routes: Routes = [
  {
    path: '',
    component: EditarPendientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditarPendientePageRoutingModule {}
