import { Component, OnInit } from '@angular/core';

import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.page.html',
  styleUrls: ['./noticias.page.scss'],
})
export class NoticiasPage implements OnInit {


  private noticias;
  private todasNoticias = new Array();

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private authService: AuthService) { }

  ngOnInit() {
    this.getNoticias();
  }

  async getNoticias(){
    this.authService.getID().then(idUsuario =>{
    this.loader.presentLoading();
    let params = {
      "idUsuario": idUsuario.toString()
    };
    let res = this.api.post("obtenerAnuncios", params).subscribe(response => {
      this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.noticias = res['data'];
        console.log(this.noticias);
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
    
  }
  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }


}
