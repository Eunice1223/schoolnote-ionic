import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ActionSheetController, AlertController, NavController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Photo, PhotoService } from '../../services/photo.service';

@Component({
  selector: 'app-agregar-foto',
  templateUrl: './agregar-foto.page.html',
  styleUrls: ['./agregar-foto.page.scss'],
})
export class AgregarFotoPage{

  private idMateria;
  private idGrupo;
  private idUsuario;
  private photoUri;
  constructor(
    private api: ApiSchoolnoteService,
    private loader: LoaderService,
    private route: ActivatedRoute,  
    private router: Router,
    private navCtrl: NavController,
    public photoService: PhotoService, 
    public actionSheetController: ActionSheetController,
    private authService: AuthService,
    private alertController: AlertController,
    private androidPermissions: AndroidPermissions) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.idMateria = this.router.getCurrentNavigation().extras.state.idMateria;
          this.idGrupo = this.router.getCurrentNavigation().extras.state.idGrupo;
        }
      });
    }

  async ngOnInit() {
    this.authService.getID().then(idUsuario =>{
      this.idUsuario = idUsuario;
    });
    await this.photoService.loadSaved();
  }

  public async showActionSheet(photo: Photo, position: number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Foto',
      buttons: [
      {
        text: 'Subir',
        icon: 'send-outline',
        role: 'cancel',
        handler: () => {
          this.uploadPhoto(position);
         }
      },{
        text: 'Borrar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
        }
      },  {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
         }
      }]
    });
    await actionSheet.present();
  }
  async uploadPhoto(position){
    await this.loader.presentLoading();
    const file = await this.photoService.getPhotoFile(position);
    //const filePhoto = await fetch(file.data);
      const blob = this.getBlob(file, ".jpg")
      let formData = new FormData();
      formData.append("idUsuario", this.idUsuario.toString());
      if (this.idMateria != undefined){
        formData.append("idMateria", this.idMateria.toString());
      }
      if (this.idGrupo != undefined){
        formData.append("idGrupo", this.idGrupo.toString());
      }
      formData.append('archivo', blob, 'archivo.jpg');

      this.api.postMultipart("agregarMultimedia", formData).subscribe(
        res =>{
          this.loader.dismissLoader();
          if(res['data'] != undefined){
            this.showError("Se ha guardado el archivo");
            this.navCtrl.pop(); 
          } else if(res['message'] != undefined)
            this.showError(res['message']);
        }, 
        error =>{
          this.loader.dismissLoader();
          console.log(error);
          this.showError("Error al conectarse al servidor");
        })
  }
  private getBlob(b64Data:string, contentType:string, sliceSize:number= 512) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    let byteCharacters = atob(b64Data);
    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        let slice = byteCharacters.slice(offset, offset + sliceSize);

        let byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        let byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }
    let blob = new Blob(byteArrays, {type: contentType});
    return blob;
}
checkPermissions(){
  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
    result => {
      if (result.hasPermission) {
        this.addPhotoToGallery();
      } else {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA).then(result => {
          if (result.hasPermission) {
          this.addPhotoToGallery();
          }
        });
      }
    },
    err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
  );

}
  addPhotoToGallery() {
    if (this.photoService.photos.length == 0){
      this.photoService.addNewToGallery();
    } else {
      this.showError("Solo se puede subir una foto, elimina la actual y toma una nueva");
    }
  }
  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }
}
