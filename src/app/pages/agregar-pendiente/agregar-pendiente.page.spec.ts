import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgregarPendientePage } from './agregar-pendiente.page';

describe('AgregarPendientePage', () => {
  let component: AgregarPendientePage;
  let fixture: ComponentFixture<AgregarPendientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarPendientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgregarPendientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
