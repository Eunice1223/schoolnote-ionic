import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileInfoService } from 'src/app/services/profile/profile-info.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@Component({
  selector: 'app-agregar-pendiente',
  templateUrl: './agregar-pendiente.page.html',
  styleUrls: ['./agregar-pendiente.page.scss'],
})
export class AgregarPendientePage implements OnInit {


  private fecha;
  private idGrupo;
  private descripcion;
  private titulo;


  private color;
  private materias = new Array();
  private materiasDia = new Array();

  fechaPicker: Date = new Date();
  

  idMateria: any;
  constructor(
    private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private authService: AuthService,
    private router: Router,
    private route:ActivatedRoute,
    private profileInfo: ProfileInfoService,
    private datePicker: DatePicker
  ) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.idMateria = this.router.getCurrentNavigation().extras.state.idMateria;
        console.log(this.idMateria);
      }
    });

  }

  ngOnInit() {
  }

  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }

  validateForm(){
    if(this.titulo != undefined && this.fecha != undefined && this.descripcion != undefined ){
        this.agregarPendiente();
        this.router.navigate(["/tabs/pendientes"]);
    } else {
      this.showError("Por favor llena todos los campos");
    }
  }

  cambioFecha(event){
    console.log('ionChange', event);
    console.log('Date', new Date(event.detail.value));
  }
  

  agregarPendiente(){
    this.authService.getID().then(idUsuario =>{
      //this.loader.presentLoading();
    let params = {
      "titulo": this.titulo,
      "fecha": this.fecha,
      "descripcion": this.descripcion,
      "idUsuario": idUsuario.toString(),
      "idMateria": this.idMateria.toString()
    };
    let res = this.api.post("agregarPendiente", params).subscribe(response => {
      //this.loader.dismissLoader();
      console.log(response);
      console.log(response.toString());
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        //this.nombre = "";
                                                  //this.routerLink="/..";
                                                  //this.nav.push(Page1)
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      //this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
    
  }

}
