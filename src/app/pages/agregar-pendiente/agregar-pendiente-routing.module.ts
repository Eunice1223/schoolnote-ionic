import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgregarPendientePage } from './agregar-pendiente.page';

const routes: Routes = [
  {
    path: '',
    component: AgregarPendientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgregarPendientePageRoutingModule {}
