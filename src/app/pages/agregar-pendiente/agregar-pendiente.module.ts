import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarPendientePageRoutingModule } from './agregar-pendiente-routing.module';

import { AgregarPendientePage } from './agregar-pendiente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarPendientePageRoutingModule
  ],
  declarations: [AgregarPendientePage]
})
export class AgregarPendientePageModule {}
