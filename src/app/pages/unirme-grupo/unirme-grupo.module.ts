import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnirmeGrupoPageRoutingModule } from './unirme-grupo-routing.module';

import { UnirmeGrupoPage } from './unirme-grupo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UnirmeGrupoPageRoutingModule
  ],
  declarations: [UnirmeGrupoPage]
})
export class UnirmeGrupoPageModule {}
