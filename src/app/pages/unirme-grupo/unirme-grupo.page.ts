import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-unirme-grupo',
  templateUrl: './unirme-grupo.page.html',
  styleUrls: ['./unirme-grupo.page.scss'],
})
export class UnirmeGrupoPage implements OnInit {

  private horario = new Array();
  private horarioGlobal = new Array();
  private horarioDia = new Array();
  private materias = new Array();
  private materiasDia = new Array();
  private idMateriaSeleccionada = 0;
  private codigo;
  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private authService: AuthService) { }

    ngOnInit() {
      this.getMaterias();
    }
  
    async getMaterias(){
  
      this.authService.getID().then(idUsuario =>{
        //this.loader.presentLoading();
        let params = { 
          "idUsuario": idUsuario.toString() 
        };
        let res = this.api.post("obtenerListaMaterias", params).subscribe(response => {
          //this.loader.dismissLoader();
          let res = JSON.parse(response.toString());
          if(res['data'] != undefined){
            this.materias = res['data'];
            console.log(this.materias);
            //$('#selectMaterias').value = this.materias[0].idMateria;
            this.cargarMaterias();
          } else if(res['message'] != undefined)
            this.showError(res['message']);
         }, error => {
          this.loader.dismissLoader();
          console.log(error);
          this.showError("Error al conectarse al servidor");
        });
        });
  
    }
    cargarMaterias(){
      let listaMateria =  this.materias;
      console.log(listaMateria);
  
      while (this.materiasDia.length > 0)
         this.materiasDia.pop();
  
         listaMateria.forEach(materiaItem => {
          this.materiasDia.push(materiaItem);
    });
  
    }
    async unirseGrupo(){
      await this.loader.presentLoading();
      this.authService.getID().then(idUsuario =>{
        let params = { 
          "idUsuario": idUsuario.toString(),
          "idMateria": this.idMateriaSeleccionada.toString(),
          "codigo": this.codigo
        };
        let res = this.api.post("unirseGrupo", params).subscribe(response => {
          this.loader.dismissLoader();
          let res = JSON.parse(response.toString());
          if(res['message'] != undefined)
            this.showError(res['message']);
         }, error => {
          this.loader.dismissLoader();
          console.log(error);
          this.showError("Error al conectarse al servidor");
        });
        });
  
    }
  async showError(message){
      const alert = await this.alertController.create({
      subHeader: message,
      buttons: ['Ok']
     });
     await alert.present(); 
   }
  
  cambioMateria($event){
    console.log($event.target.value);
    this.idMateriaSeleccionada = $event.target.value;
    console.log(this.idMateriaSeleccionada);
  }
  
  }