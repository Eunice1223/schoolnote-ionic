import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnirmeGrupoPage } from './unirme-grupo.page';

const routes: Routes = [
  {
    path: '',
    component: UnirmeGrupoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnirmeGrupoPageRoutingModule {}
