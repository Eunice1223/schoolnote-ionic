import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UnirmeGrupoPage } from './unirme-grupo.page';

describe('UnirmeGrupoPage', () => {
  let component: UnirmeGrupoPage;
  let fixture: ComponentFixture<UnirmeGrupoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnirmeGrupoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UnirmeGrupoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
