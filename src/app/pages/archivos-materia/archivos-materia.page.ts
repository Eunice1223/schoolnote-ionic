
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../../services/auth/auth.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { cordova } from '@ionic-native/core';

import { PopoverController } from '@ionic/angular';  
import {  PopoverEtiquetaPage} from '../../popover-etiqueta/popover-etiqueta.page';  
import { PreviewAnyFile } from '@ionic-native/preview-any-file/ngx';
@Component({
  selector: 'app-archivos-materia',
  templateUrl: './archivos-materia.page.html',
  styleUrls: ['./archivos-materia.page.scss'],
})
export class ArchivosMateriaPage implements OnInit {

  private idUsuario;
  inAppBrowserRef;
  private materia;
  private idMateria;
  private multimedia :  String[];
  private multimediaFilter:  String[];
  private isAdmin = false;
  private hasGroup = false;
  private groupFilesOnly = false;
  private isSelecting = false;
  private selectedIds = new Array();
  private etiqueta
  textoBuscar= '';

  constructor(
    private api: ApiSchoolnoteService,
    private loader: LoaderService,
    private alertController: AlertController,
    private route: ActivatedRoute, 
    private router: Router,
    private iab: InAppBrowser,
    private authService: AuthService,
    private popover:PopoverController,
    private fileViewer: PreviewAnyFile) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.materia= JSON.parse(this.router.getCurrentNavigation().extras.state.materia);
          console.log(this.materia);
          this.hasGroup = this.materia.idGrupo != null;
          this.groupFilesOnly = this.router.getCurrentNavigation().extras.state.groupFilesOnly;
        }
      });
     }

  ngOnInit() {
    
    this.getMultimedia();
    this.authService.getID().then(id =>{
      this.idUsuario = id
      this.isAdmin = this.materia.admin == this.idUsuario;
    })
    this.hasGroup = this.materia.idGrupo != null;
    this.idMateria=this.materia.idMateria;
  }

  async getMultimedia(){
    this.authService.getID().then(idUsuario =>{
    //this.loader.presentLoading();
    let params = {
      "idMateria": this.idMateria.toString()
    };
    if(this.groupFilesOnly){
      params["idGrupo"] = this.materia.idGrupo.toString();
    } else {
      params["idUsuario"] = idUsuario.toString();
    }
    let res = this.api.post("obtenerMultimediaM", params).subscribe(response => {
      //this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.multimedia = res['data'];
        //this.multimedia.reverse();
        console.log(this.multimedia);
        this.multimediaFilter=this.multimedia;
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      //this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
    
  }

  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }
select(){
  this.isSelecting = !this.isSelecting;
  if(!this.isSelecting){
    this.selectedIds = [];
  }
}
addRemoveID(idArchivoSeleccionado){
  if (this.selectedIds.includes(idArchivoSeleccionado)){
    this.selectedIds.splice(this.selectedIds.indexOf(idArchivoSeleccionado), 1);
  } else {
    this.selectedIds.push(idArchivoSeleccionado);
  }
  console.log(this.selectedIds);
}
async viewFile(urlFile){
  let url = urlFile.replace(" ", "%20");
  this.inAppBrowserRef = this.iab.create(url,'_blank','location=no,hardwareback=yes,toolbar=yes,enableViewportScale=yes');
  if(this.inAppBrowserRef.on('loadstop') != undefined){
    this.inAppBrowserRef.on('loadstop').subscribe(event => this.loadStopedCallBack());
    this.inAppBrowserRef.on('exit').subscribe(event => this.exitCallBack());
  } else {
    this.loader.dismissLoader();
  }
}
loadStopedCallBack() {
  if (this.inAppBrowserRef != undefined) {
    //this.inAppBrowserRef.insertCSS({ code: "body{font-size: 25px;}" });
    this.inAppBrowserRef.show();
  }
  this.loader.dismissLoader();
}
exitCallBack() {
  this.loader.dismissLoader();
}

buscar(event){
  //console.log(event);
  this.textoBuscar = event.detail.value;
}
 /*  buscar(ev: CustomEvent){
   console.log(ev);
   let val = ev.detail.value;
   if(val && val.trim() !== ''){
     this.multimediaFilter = this.multimediaFilter.filter(term =>
      //term.toLocaleLowerCase().indexOf(val.toLocaleLowerCase()) > -1)
      term.indexOf(val) > -1)
   }
   else
    this.multimediaFilter = this.multimedia;
 } */

 async presentAlertConfirm() {
   if(this.isSelecting){
  const alert = await this.alertController.create({
    header: 'Comartir',
    message: '¿Seguro que deseas compartir los archivos seleccionados al grupo de la materia '+ this.materia.nombreMateria +" ?",
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          this.select();
        }
      }, {
        text: 'Aceptar',
        handler: () => {
          this.compartirArchivos();
        }
      }
    ]
  });

  await alert.present();
 }
}

 agregarEtiqueta(){

  this.popover.create({
    component:PopoverEtiquetaPage,
    showBackdrop:false,
    componentProps:{
      ids : this.selectedIds
    }
    }).then((popoverElement)=>{
      popoverElement.present();
    })
 }

 eliminarArchivos(){
  for (let id in this.selectedIds){
    let params = {
      "idUsuario": this.idUsuario.toString(),
      "idMultimedia": this.selectedIds[id].toString(),
      "idGrupo":this.materia.idGrupo.toString(),
    };
    let res = this.api.post("eliminarMultimedia", params).subscribe(response => {
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.getMultimedia();
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
  }
 }

async compartirArchivos(){
  await this.loader.presentLoading();
  this.authService.getID().then(idUsuario =>{
    let params = {
      "idUsuario": this.idMateria.toString(),
      "idGrupo": this.materia.idGrupo.toString(),
      "idsMultimedia": this.selectedIds.toString()
    };
    console.log(params);
    let res = this.api.post("compartirMultimedia", params).subscribe(response => {
      this.select();
      this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
}
async presentAlertDelete() {
  if(this.isSelecting){
 const alert = await this.alertController.create({
   header: 'Eliminar',
   message: '¿Seguro que deseas eliminar los archivos seleccionados?',
   buttons: [
     {
       text: 'Cancelar',
       role: 'cancel',
       cssClass: 'secondary',
       handler: (blah) => {
         this.select();
       }
     }, {
       text: 'Aceptar',
       handler: () => {
         this.eliminarArchivos();
       }
     }
   ]
 });

 await alert.present();
}
}
}

