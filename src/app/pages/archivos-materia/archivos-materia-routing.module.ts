import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArchivosMateriaPage } from './archivos-materia.page';

const routes: Routes = [
  {
    path: '',
    component: ArchivosMateriaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArchivosMateriaPageRoutingModule {}
