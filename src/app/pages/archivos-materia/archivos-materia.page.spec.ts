import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ArchivosMateriaPage } from './archivos-materia.page';

describe('ArchivosMateriaPage', () => {
  let component: ArchivosMateriaPage;
  let fixture: ComponentFixture<ArchivosMateriaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivosMateriaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ArchivosMateriaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
