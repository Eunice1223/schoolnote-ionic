import { Component, OnInit } from '@angular/core';

import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NavigationExtras} from '@angular/router';

@Component({
  selector: 'app-archivos',
  templateUrl: './archivos.page.html',
  styleUrls: ['./archivos.page.scss'],
})
export class ArchivosPage implements OnInit {

  private materias = new Array();
  private multimediaReciente = new Array();

  constructor(
    private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
    this.getMaterias();
    this.getMultimedia();
  }

  async getMaterias(){
    await this.loader.presentLoading();
    this.authService.getID().then(idUsuario =>{
    let params = {
      "idUsuario": idUsuario.toString()
    };
    let res = this.api.post("obtenerListaMaterias", params).subscribe(response => {
      this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.materias = res['data'];
        console.log(this.materias);
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
    
  }

  async getMultimedia(){
    //await this.loader.presentLoading();
    this.authService.getID().then(idUsuario =>{
    let params = {
      "idUsuario": idUsuario.toString()
    };
    let res = this.api.post("obtenerMultimediaReciente", params).subscribe(response => {
      //this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.multimediaReciente = res['data'];
        //this.multimediaReciente.reverse();
        console.log(this.multimediaReciente);
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      //this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
    
  }

  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }

 openDetail(id){
  this.getInfoMateria(id);
 }
 getInfoMateria(id){
  let params = {
    "idMateria": id.toString()
  };
  let res = this.api.post("obtenerInformacionMateria", params).subscribe(response => {
    let res = JSON.parse(response.toString());
    if(res['data'] != undefined){
      let navigationExtras: NavigationExtras = { state:{materia: JSON.stringify(res['data'])}};
      this.router.navigate(['archivos-materia'], navigationExtras);
    } else if(res['message'] != undefined){}
      this.showError(res['message']);
    }, error => {
    console.log(error);
    this.showError("Error al conectarse al servidor");
  });
}

}
