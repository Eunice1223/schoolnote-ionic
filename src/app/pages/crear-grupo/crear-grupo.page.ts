import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-crear-grupo',
  templateUrl: './crear-grupo.page.html',
  styleUrls: ['./crear-grupo.page.scss'],
})
export class CrearGrupoPage implements OnInit {

  private materias = new Array();
  private materiasDia = new Array();
  private idMateriaSeleccionada = 0;

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private authService: AuthService) { }

  ngOnInit() {
    this.getMaterias();
  }

  async getMaterias(){
    this.authService.getID().then(idUsuario =>{
      let params = { 
        "idUsuario": idUsuario.toString() 
      };
      let res = this.api.post("obtenerListaMaterias", params).subscribe(response => {
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.materias = res['data'];
          console.log(this.materias);
          this.cargarMaterias();
        } else if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });
  }
  cargarMaterias(){
    let listaMateria =  this.materias;
    console.log(listaMateria);

    while (this.materiasDia.length > 0)
       this.materiasDia.pop();

       listaMateria.forEach(materiaItem => {
        this.materiasDia.push(materiaItem);
  });

  }
  async crearGrupo(){
    await this.loader.presentLoading();
    this.authService.getID().then(idUsuario =>{
      let params = { 
        "idUsuario": idUsuario.toString(),
        "idMateria": this.idMateriaSeleccionada.toString()
      };
      let res = this.api.post("crearGrupo", params).subscribe(response => {
        this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });

  }
async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }

cambioMateria($event){
  console.log($event.target.value);
  this.idMateriaSeleccionada = $event.target.value;
  console.log(this.idMateriaSeleccionada);
}
}
