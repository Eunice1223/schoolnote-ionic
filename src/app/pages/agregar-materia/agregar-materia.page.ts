import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileInfoService } from 'src/app/services/profile/profile-info.service';

@Component({
  selector: 'app-agregar-materia',
  templateUrl: './agregar-materia.page.html',
  styleUrls: ['./agregar-materia.page.scss'],
})
export class AgregarMateriaPage implements OnInit {

  private nombre;
  private color;
  private materias = new Array();
  private materiasDia = new Array();

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private authService: AuthService,
    private router: Router,
    private profileInfo: ProfileInfoService) { }

  ngOnInit() { 
    this.getMaterias();
  }

  validateForm(){
    if(this.nombre != undefined && this.color != undefined ){
        this.agregarMateria()
    } else {
      this.showError("Por favor llena todos los campos");
    }
  }

  agregarMateria(){
    this.authService.getID().then(idUsuario =>{
      //this.loader.presentLoading();
    let params = {
      "nombre": this.nombre,
      "color": this.color,
      "idUsuario": idUsuario.toString()
    };
    let res = this.api.post("agregarMateria", params).subscribe(response => {
      //this.loader.dismissLoader();
      console.log(response);
      console.log(response.toString());
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.nombre = "";
        this.getMaterias();
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      //this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
    
  }
  getMaterias(){
    this.authService.getID().then(idUsuario =>{
      //this.loader.presentLoading();
      let params = { 
        "idUsuario": idUsuario.toString() 
      };
      let res = this.api.post("obtenerListaMaterias", params).subscribe(response => {
        //this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.materias = res['data'];
          console.log(this.materias);
          this.cargarMaterias();
        } else if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });
  }

  cargarMaterias(){
    let listaMateria =  this.materias;
    console.log(listaMateria);

    while (this.materiasDia.length > 0)
       this.materiasDia.pop();

       listaMateria.forEach(materiaItem => {
        this.materiasDia.push(materiaItem);
  });
  }
  async showError(message){
      const alert = await this.alertController.create({
      subHeader: message,
      buttons: ['Ok']
     });
     await alert.present(); 
   }

   openDetail(id){
    this.getInfoMateria(id);
   }
   getInfoMateria(id){
    let params = {
      "idMateria": id.toString()
    };
    let res = this.api.post("obtenerInformacionMateria", params).subscribe(response => {
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        let navigationExtras: NavigationExtras = { state:{materia: JSON.stringify(res['data'])}};
        this.router.navigate(['tabs-materia'], navigationExtras);
      } else if(res['message'] != undefined){}
        //this.showError(res['message']);
      }, error => {
      console.log(error);
      //this.showError("Error al conectarse al servidor");
    });
  }

  editar(id, nombre, color){
    let navigationExtras: NavigationExtras = { state : {idMateria: id, nombre:nombre, color:color}};
        this.router.navigate(['editar-materia'], navigationExtras);
  }
  
  eliminar(id){
    let params = {
      "idMateria": id.toString()
    };
    let res = this.api.post("eliminarMateria", params).subscribe(response => {
      this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.showError(res['message']);
      } else if(res['message'] != undefined)
        this.showError(res['message']);
        this.getMaterias();
     }, error => {
      this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
  }

}
