import { Component, OnInit } from '@angular/core';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AlertController} from '@ionic/angular';
import {Router} from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileInfoService } from 'src/app/services/profile/profile-info.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private email;
  private pass;

  
  constructor( private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router, 
    private authService: AuthService,
    private profileInfo: ProfileInfoService) { }

  ngOnInit() {
  }

  async login(){
    await this.loader.presentLoading();
    let params = {
      "email": this.email,
      "pass": this.pass
    };
    let res = this.api.post("login", params).subscribe(response => {
      this.loader.dismissLoader();
      console.log(response);
      console.log(response.toString());
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.goToMain(res['data']);
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
  }
  goToMain(data){
    this.authService.login(data['idUsuario']);
    this.profileInfo.saveProfileInfo(data);
    this.router.navigate(["tabs"]);
  }
  goToRegister(data){
    this.router.navigate(["registro"]);
  }
  async showError(message){
      const alert = await this.alertController.create({
      subHeader: message,
      buttons: ['Ok']
     });
     await alert.present(); 
   }
}
