import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CambiarAdminPage } from './cambiar-admin.page';

const routes: Routes = [
  {
    path: '',
    component: CambiarAdminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CambiarAdminPageRoutingModule {}
