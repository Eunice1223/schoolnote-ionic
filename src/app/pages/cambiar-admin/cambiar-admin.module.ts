import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CambiarAdminPageRoutingModule } from './cambiar-admin-routing.module';

import { CambiarAdminPage } from './cambiar-admin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CambiarAdminPageRoutingModule
  ],
  declarations: [CambiarAdminPage]
})
export class CambiarAdminPageModule {}
