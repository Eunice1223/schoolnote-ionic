
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-cambiar-admin',
  templateUrl: './cambiar-admin.page.html',
  styleUrls: ['./cambiar-admin.page.scss'],
})
export class CambiarAdminPage implements OnInit {

  private idGrupo;
  private users;
  
  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute, 
    private authService: AuthService,
    private navCtrl: NavController) { 
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.idGrupo= this.router.getCurrentNavigation().extras.state.idGrupo;
          this.users = JSON.parse(this.router.getCurrentNavigation().extras.state.usuarios);
        }
      });
    }



  ngOnInit() {
    console.log(this.idGrupo)
    console.log(this.users)
  }

  async cambiarAdmin(idUsuarioNuevo) {
    let params = {
      "idGrupo": this.idGrupo.toString(),
      "idUsuario": idUsuarioNuevo.toString()
    };
    let res = this.api.post("cambiarAdminGrupo", params).subscribe(response => {
      let res = JSON.parse(response.toString());
      if(res['success'] == 1){
          this.showError(res['message']);
          this.router.navigate(['tabs-materia']);
      } else if(res['message'] != undefined){}
          //this.showError(res['message']);
      }, error => {
        console.log(error);
        //this.showError(error);
    });
  }

  async showError(message) {
    const alert = await this.alertController.create({
      subHeader: message,
      buttons: ['Ok']
    });
    await alert.present();
  }

}
