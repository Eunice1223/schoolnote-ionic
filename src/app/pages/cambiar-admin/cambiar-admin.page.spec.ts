import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CambiarAdminPage } from './cambiar-admin.page';

describe('CambiarAdminPage', () => {
  let component: CambiarAdminPage;
  let fixture: ComponentFixture<CambiarAdminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambiarAdminPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CambiarAdminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
