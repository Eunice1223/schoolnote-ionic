import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MateriaConfiguracionPage } from './materia-configuracion.page';

const routes: Routes = [
  {
    path: '',
    component: MateriaConfiguracionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MateriaConfiguracionPageRoutingModule {}
