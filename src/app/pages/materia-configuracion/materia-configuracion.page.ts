import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-materia-configuracion',
  templateUrl: './materia-configuracion.page.html',
  styleUrls: ['./materia-configuracion.page.scss'],
})
export class MateriaConfiguracionPage implements OnInit {

  private materia;
  private users;
  private idGrupo;
  private isAdmin = false;
  private idUsuario;

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute, 
    private authService: AuthService) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.materia= JSON.parse(this.router.getCurrentNavigation().extras.state.materia);
        }
      });
     }

  ngOnInit() {
    this.getMembers();
    this.authService.getID().then(id =>{
      this.idUsuario = id
      this.isAdmin = this.materia.admin == this.idUsuario;
    })
  }
  getMembers(){
    this.loader.presentLoading();
    let params = {
      "idGrupo": this.materia.idGrupo.toString()
    };
    
    let res = this.api.post("obtenerUsuariosGrupo", params).subscribe(response => {
      this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.users = res['data'];
      } else if(res['message'] != undefined){
        this.showError(res['message']);
      }
      }, error => {
      this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
      });
  }

  editarAdmin(){
    let navigationExtras: NavigationExtras = { state:{idGrupo: JSON.stringify(this.materia.idGrupo),usuarios: JSON.stringify(this.users)}};
      this.router.navigate(['cambiar-admin'], navigationExtras);
  }

  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }
}
