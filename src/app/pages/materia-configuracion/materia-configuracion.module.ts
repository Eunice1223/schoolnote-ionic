import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MateriaConfiguracionPageRoutingModule } from './materia-configuracion-routing.module';

import { MateriaConfiguracionPage } from './materia-configuracion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MateriaConfiguracionPageRoutingModule
  ],
  declarations: [MateriaConfiguracionPage]
})
export class MateriaConfiguracionPageModule {}
