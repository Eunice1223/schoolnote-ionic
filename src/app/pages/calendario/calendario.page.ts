import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.page.html',
  styleUrls: ['./calendario.page.scss'], 
})

export class CalendarioPage implements OnInit { 

  private horario = new Array();
  private horarioGlobal = new Array();
  private horarioDia = new Array();
  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private authService: AuthService,
    private navCtrl: NavController) { }


    ngOnInit() {
      this.getHorario()
    }
  
    async getHorario(){
      await this.loader.presentLoading();
      this.authService.getID().then(idUsuario =>{
      let params = { 
        "idUsuario": idUsuario.toString() 
      };
      let res = this.api.post("obtenerHorario", params).subscribe(response => {
        this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.horarioGlobal = res['data'];
          console.log(this.horarioGlobal);
          this.cargarDia(1,this);
        } else if(res['message'] != undefined)
        if(res['message']!='No se encontró horario para el usuario.')
          this.showError(res['message']);
        else
          this.router.navigate(['/sin-horario']);
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });
    }

      async showError(message){
        const alert = await this.alertController.create({
        subHeader: message,
        buttons: ['Ok']
       });
       await alert.present(); 
     }
    

    cargarDia(dia, tab){
      let listaHorario =  this.horarioGlobal;
     console.log(listaHorario);

     while (this.horarioDia.length > 0)
        this.horarioDia.pop();

        while (this.horario.length > 0)
        this.horario.pop();

      listaHorario.forEach(horarioItem => {
       let diaHorario = horarioItem.dia;
      

        if (horarioItem.horaInicio.split('T').length > 1){
          horarioItem.horaInicio = horarioItem.horaInicio.split("T")[1].slice(0,-8);
        }
       if(diaHorario == dia){
         this.horarioDia.push(horarioItem);
       } else {
         this.horario.push(horarioItem);
       }
   });
      
    }

    openDetail(id){
      this.getInfoMateria(id);
     }
     getInfoMateria(id){
      let params = {
        "idMateria": id.toString()
      };
      let res = this.api.post("obtenerInformacionMateria", params).subscribe(response => {
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          let navigationExtras: NavigationExtras = { state:{materia: JSON.stringify(res['data'])}};
          this.router.navigate(['tabs-materia'], navigationExtras);
        } else if(res['message'] != undefined){} 
          this.showError(res['message']);
        }, error => {
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
    }

}
