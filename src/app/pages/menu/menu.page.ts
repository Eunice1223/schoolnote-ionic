import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileInfoService } from 'src/app/services/profile/profile-info.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  private user;
  constructor(
    private authService: AuthService,
    private router: Router,
    private profileInfo: ProfileInfoService
  ) { }

  ngOnInit() {
    this.getUserInfo();
  }
  getUserInfo(){
    this.profileInfo.getProfileInfo().then(info =>{
      this.user = info
      if(this.user.foto == null || this.user.foto == "null"){
        this.user.foto = "../../../assets/user.png"
      }
    });
  }
  logout(){
    this.authService.logout()
    //this.router.navigate(["login"]);
  }
}
