import { Component, OnInit } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FileEntry, File} from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AlertController, NavController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileInfoService } from 'src/app/services/profile/profile-info.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  private user; 
  private name;
  private lastname;
  private pass;
  private idUsuario;
  private filesPath;
  private fileType;
  private filesName;
  private isEditing = false;
  private foto;

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private profileInfo: ProfileInfoService,
    private authService: AuthService, 
    private fileChooser: FileChooser,
    private filePath: FilePath,
    private file: File,
    private navCtrl: NavController,
    private transfer: FileTransfer,
    private androidPermissions: AndroidPermissions) { }

  ngOnInit() {
    this.getUserInfo();
    this.authService.getID().then(id=>{
      this.idUsuario = id;
    })
  }
  getUserInfo(){
    this.profileInfo.getProfileInfo().then(info =>{
      this.user = info
      if(this.user.foto == null || this.user.foto == "null"){
        this.user.foto = "../../../assets/user.png"
      }
      this.user.foto = this.user.foto.replace(" ", "%20");
    });
  }
  edit(){
    this.isEditing = true;
  }
  stopEditing(){
    this.isEditing = false;
  }
  async updateProfileInfo(){
      await this.loader.presentLoading();
      let params = {
        "idUsuario": this.idUsuario.toString(),
        "pass": this.pass,
        "name": this.name,
        "lastname": this.lastname,
      };
      let res = this.api.post("editarPerfil", params).subscribe(response => {
        this.isEditing = false;
        this.loader.dismissLoader();
        console.log(response);
        console.log(response.toString());
        let res = JSON.parse(response.toString());
        if (res['data']!= undefined){
          this.user = res['data'];
          this.profileInfo.updateProfileInfo(res['data']).then(info=>{
            this.getUserInfo();
          });
        }
        if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        this.isEditing = false;
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
  }
  openFileSelector(){
    this.fileChooser.open().then(uri =>
      {
        console.log(uri);
        this.filePath.resolveNativePath(uri).then(filePath =>
          {
            this.filesPath = filePath;
            this.file.resolveLocalFilesystemUrl(filePath).then(fileInfo =>
              {
                console.log("file info " + fileInfo);
                let files = fileInfo as FileEntry;
                files.file(success =>
                  {
                    this.fileType   = success.type;
                    this.filesName  = success.name;
                    const capacitorFile = Capacitor.convertFileSrc(filePath);
                    let name = capacitorFile.substring(capacitorFile.lastIndexOf('/')+1, capacitorFile.length);
                    this.transferFileToServer(uri, name);
                  });
              },err =>
              {
                console.log("error obteniendo archivo " + err);
                throw err;
              });
          },err =>
          {
            console.log("error obteniendo archivo " + err);
            throw err;
          });
      },err =>
      {
        console.log(err);
        throw err;
      });
  }
  async transferFileToServer(uri, name){
    console.log(name);
    await this.loader.presentLoading();
    var parameters = {
      "idUsuario": this.idUsuario.toString()
    };
    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {
      fileKey: 'archivo',
      fileName: name+this.fileType,
      httpMethod: "POST",
      mimeType: this.fileType,
      headers: this.api.httpOptionsMultipart,
      params:parameters
   };
   console.log(parameters.toString());
   fileTransfer.upload(uri, this.api.urlBase+"subirFotoPerfil", options)
    .then((data) => {
      console.log(data);
      if (data['data']!= undefined){
        this.user = data['data'];
        this.user.foto = this.user.foto.replace(" ", "%20");
        this.profileInfo.updateProfileInfo(data['data']).then(info=>{
          this.getUserInfo();
        });
      }
      this.loader.dismissLoader();
      this.showError("Se ha actualizado la información");
      this.navCtrl.pop();
    }, (err) => {
      console.log(err);
      this.loader.dismissLoader();
      this.showError("Ocurrió un error al guardar el archivo");
    });
  }
  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }
}
