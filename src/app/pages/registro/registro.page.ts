import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileInfoService } from 'src/app/services/profile/profile-info.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  
  private email;
  private pass;
  private name;
  private lastname;

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private authService: AuthService,
    private profileInfo: ProfileInfoService) { }

  ngOnInit() {
  }
  validateForm(){
    if(this.email != undefined && this.pass != undefined && this.name != undefined && this.lastname != undefined){
      if(this.email.untouched && this.email.invalid)
        this.showError("Por favor ingresa un email válido");
      else
        this.register()
    } else {
      this.showError("Por favor llena todos los campos");
    }
  }
  register(){
    this.loader.presentLoading();
    let params = {
      "email": this.email,
      "pass": this.pass,
      "name": this.name,
      "lastname": this.lastname
    };
    let res = this.api.post("registro", params).subscribe(response => {
      this.loader.dismissLoader();
      console.log(response);
      console.log(response.toString());
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.goToMain(res['data']);
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
  }
  goToMain(data){
    this.authService.login(data['idUsuario']);
    this.profileInfo.saveProfileInfo(data);
  }
  async showError(message){
      const alert = await this.alertController.create({
      subHeader: message,
      buttons: ['Ok']
     });
     await alert.present(); 
   }
}
