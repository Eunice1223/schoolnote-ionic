
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../../services/auth/auth.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { PopoverController } from '@ionic/angular';  
import { LoaderService } from 'src/app/services/api/loader.service';



@Component({
  selector: 'app-popovercomponent',
  templateUrl: './popovercomponent.page.html',
  styleUrls: ['./popovercomponent.page.scss'],
})
export class PopovercomponentPage implements OnInit {
  private ids;
  private etiqueta;

  constructor(
    private api: ApiSchoolnoteService,
    private loader: LoaderService,
    private alertController: AlertController,
    private route: ActivatedRoute, 
    private router: Router,
    private authService: AuthService,
    private popover:PopoverController) { }

  ngOnInit() {
    //this.ids = this.navParams.get('ids');
    console.log("en pop over")
    console.log(this.ids);
  }

  guardarEtiqueta(){
    let miApi = this.api
    this.ids.forEach(function (value){
      let params = {
        "idMultimedia": value.toString(),
        "etiqueta": value.toString()
      };
      this.authService.getID().then(idUsuario =>{
      let res = this.api.post("agregarEtiqueta", params).subscribe(response => {
        //this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.multimedia = res['data'];
          console.log(this.etiqueta);
          this.dismiss();
          //this.multimedia.reverse();
        } else if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        //this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
     });
    });
  }

  dismiss()
  {
    this.popover.dismiss(this.etiqueta);
  }

}
