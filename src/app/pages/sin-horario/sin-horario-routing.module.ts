import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SinHorarioPage } from './sin-horario.page';

const routes: Routes = [
  {
    path: '',
    component: SinHorarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SinHorarioPageRoutingModule {}
