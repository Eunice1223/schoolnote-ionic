import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SinHorarioPage } from './sin-horario.page';

describe('SinHorarioPage', () => {
  let component: SinHorarioPage;
  let fixture: ComponentFixture<SinHorarioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinHorarioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SinHorarioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
