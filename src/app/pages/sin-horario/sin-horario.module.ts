import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SinHorarioPageRoutingModule } from './sin-horario-routing.module';

import { SinHorarioPage } from './sin-horario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SinHorarioPageRoutingModule
  ],
  declarations: [SinHorarioPage]
})
export class SinHorarioPageModule {}
