import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileInfoService } from 'src/app/services/profile/profile-info.service';

@Component({
  selector: 'app-editar-materia',
  templateUrl: './editar-materia.page.html',
  styleUrls: ['./editar-materia.page.scss'],
})
export class EditarMateriaPage implements OnInit {
  private nombre;
  private color;
  private materias = new Array();
  private materiasDia = new Array();
  private idMateria;

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private route: ActivatedRoute, 
    private authService: AuthService,
    private router: Router,
    private profileInfo: ProfileInfoService) { 
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.idMateria= this.router.getCurrentNavigation().extras.state.idMateria;
          this.nombre = this.router.getCurrentNavigation().extras.state.nombre;
          this.color = this.router.getCurrentNavigation().extras.state.color;
        }
      });
    }

  ngOnInit() { 
    this.getMaterias();
  }

  validateForm(){
    if(this.nombre != undefined && this.color != undefined ){
        this.agregarMateria()
    } else {
      this.showError("Por favor llena todos los campos");
    }
  }
  editarMateria(){

  }

  agregarMateria(){
    this.authService.getID().then(idUsuario =>{
      //this.loader.presentLoading();
    let params = {
      "nombre": this.nombre,
      "color": this.color,
      "idUsuario": idUsuario.toString()
    };
    let res = this.api.post("agregarMateria", params).subscribe(response => {
      //this.loader.dismissLoader();
      console.log(response);
      console.log(response.toString());
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.nombre = "";
        this.getMaterias();
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      //this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
    
  }

  getMaterias(){
    this.authService.getID().then(idUsuario =>{
      //this.loader.presentLoading();
      let params = { 
        "idUsuario": idUsuario.toString() 
      };
      let res = this.api.post("obtenerListaMaterias", params).subscribe(response => {
        //this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.materias = res['data'];
          console.log(this.materias);
          this.cargarMaterias();
          this.editarMateria();
        } else if(res['message'] != undefined)
          this.showError(res['message']);
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });
  }

  cargarMaterias(){
    let listaMateria =  this.materias;
    console.log(listaMateria);

    while (this.materiasDia.length > 0)
       this.materiasDia.pop();

       listaMateria.forEach(materiaItem => {
        this.materiasDia.push(materiaItem);
  });
  }
  async showError(message){
      const alert = await this.alertController.create({
      subHeader: message,
      buttons: ['Ok']
     });
     await alert.present(); 
   }

   openDetail(id){
    this.getInfoMateria(id);
   }
   getInfoMateria(id){
    let params = {
      "idMateria": id.toString()
    };
    let res = this.api.post("obtenerInformacionMateria", params).subscribe(response => {
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        let navigationExtras: NavigationExtras = { state:{materia: JSON.stringify(res['data'])}};
        this.router.navigate(['tabs-materia'], navigationExtras);
      } else if(res['message'] != undefined){}
        //this.showError(res['message']);
      }, error => {
      console.log(error);
      //this.showError("Error al conectarse al servidor");
    });
  }

  guardarMateria(){
      //this.loader.presentLoading();
    let params = {
      "idMateria": this.idMateria.toString(),
      "nombre": this.nombre,
      "color": this.color
      
    };
    let res = this.api.post("editarMateria", params).subscribe(response => {
      //this.loader.dismissLoader();
      console.log(response);
      console.log(response.toString());
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.nombre = "";
        this.getMaterias();
      } else if(res['message'] != undefined)
        this.showError(res['message']);
        this.getMaterias();
     }, error => {
      //this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    
  }

}
