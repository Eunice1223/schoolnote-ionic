import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ValueTransformer } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-agregar-horario',
  templateUrl: './agregar-horario.page.html',
  styleUrls: ['./agregar-horario.page.scss'],
})
export class AgregarHorarioPage implements OnInit {

  private horario = new Array();
  private horarioGlobal = new Array();
  private horarioDia = new Array();
  private materias = new Array();
  private materiasDia = new Array();
  private idMateria;
  private dia = 1;
  private cadenaHorarios;
  private btn;
  todo: {}
  ids = 0;
  lista: Array<any> = [
    {
      idHorario: this.ids,
      horaInicio: "",
      horaFin: ""
    }
  ]

  constructor(private api: ApiSchoolnoteService,
    private route: ActivatedRoute, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private authService: AuthService) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.idMateria= JSON.parse(this.router.getCurrentNavigation().extras.state.idMateria);
        }
      });
  }

  ngOnInit() {
    this.getMaterias();
  }

  async getMaterias() {

    this.authService.getID().then(idUsuario => {
      //this.loader.presentLoading();
      let params = {
        "idUsuario": idUsuario.toString()
      };
      let res = this.api.post("obtenerListaMaterias", params).subscribe(response => {
        //this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if (res['data'] != undefined) {
          this.materias = res['data'];
          this.cargarMaterias();
          this.getHorario();
        } else if (res['message'] != undefined)
          this.showError(res['message']);
      }, error => {
        this.loader.dismissLoader();
        this.showError("Error al conectarse al servidor");
      });
    });

  }

  cargarMaterias() {
    let listaMateria = this.materias;
    console.log(listaMateria);

    while (this.materiasDia.length > 0)
      this.materiasDia.pop();

    listaMateria.forEach(materiaItem => {
      this.materiasDia.push(materiaItem);
    });

  }

  async getHorario() {
    await this.loader.presentLoading();
    this.authService.getID().then(idUsuario => {

      let params = {
        "idUsuario": idUsuario.toString()
      };

      let res = this.api.post("obtenerHorario", params).subscribe(response => {

        this.loader.dismissLoader();
        let res = JSON.parse(response.toString());

        if (res['data'] != undefined) {
          this.horarioGlobal = res['data'];
          this.cargarHorarioMateria();
        } else if (res['message'] != undefined){
          if(res['message'] != 'No se encontró horario para el usuario.')
            this.showError(res['message']);
        }

      }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
    });
  }

  cargarHorarioMateria() {
    let listaHorario = this.horarioGlobal;

    while (this.lista.length > 0)
      this.lista.pop();

    while (this.horario.length > 0)
      this.horario.pop();

    listaHorario.forEach(horarioItem => {
      let idMateriaSelec = horarioItem.idMateria;
      let diaSelect = horarioItem.dia;
      if (horarioItem.horaInicio.split('T').length > 1) {
        horarioItem.horaInicio = horarioItem.horaInicio.split("T")[1].slice(0, -8);
      }
      if (horarioItem.horaFin.split('T').length > 1) {
        horarioItem.horaFin = horarioItem.horaFin.split("T")[1].slice(0, -8);
      }
      if (idMateriaSelec == this.idMateria && diaSelect == this.dia) {
        this.lista.push(horarioItem);
      } else {
        this.horario.push(horarioItem);
      }
    });

  }

  async eliminarHorario(){

  }

  async guardarHorario() {
    this.cadenaHorarios = await this.horarios();
    this.authService.getID().then(idUsuario =>{
    if (this.cadenaHorarios == ""){ 
      this.cadenaHorarios = 0;
    }
    let params = {
      "idMateria": this.idMateria.toString(),
      "dia": this.dia.toString(),
      "rangos": this.cadenaHorarios.toString()
    };
    let res = this.api.post("editarHorario", params).subscribe(response => {
      let res = JSON.parse(response.toString());
      if(res['success'] == 1){
          this.showError(res['message']);
      } else if(res['message'] != undefined){}
          //this.showError(res['message']);
      }, error => {
        console.log(error);
        //this.showError(error);
        this.cargarHorarioMateria();
    });
  });
  }

  horarios(){
    let tamaño = this.lista.length - 1;
    let cadena = "";
    this.lista.forEach(function (value, index){
      if(value.horaInicio.includes('T')){
        cadena += value.horaInicio.split("T")[1].split("-")[0].split(".")[0] + "-" + value.horaFin.split("T")[1].split("-")[0].split(".")[0];
      }else{
        cadena += value.horaInicio + "-" + value.horaFin;
      }
      if (index != tamaño)
          cadena += ","
  
    });
    return cadena;
  }

  agregarRango() {
    this.ids--;
    this.lista.push(
      {
        id: this.ids,
        horaInicio: "",
        horaFin: ""
      })
  }

  eliminarRango($event) {
    let id = $event.target.parentElement.id, idBorrar;
    this.lista.forEach(function (value, index){
      if (value.idHorario == undefined){
        if(value.id == id){
          idBorrar = index;
        }
      }else{
        if(value.idHorario == id){
          idBorrar = index;
        }
      }
      
    });

    this.lista.splice(idBorrar,1);
  }

  recargar(){
    this.cargarHorarioMateria();
  }

  async showError(message) {
    const alert = await this.alertController.create({
      subHeader: message,
      buttons: ['Ok']
    });
    await alert.present();
  }

}


