import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarHorarioPageRoutingModule } from './agregar-horario-routing.module';

import { AgregarHorarioPage } from './agregar-horario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarHorarioPageRoutingModule
  ],
  declarations: [AgregarHorarioPage]
})
export class AgregarHorarioPageModule {}
