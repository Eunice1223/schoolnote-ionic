import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-materias',
  templateUrl: './materias.page.html',
  styleUrls: ['./materias.page.scss'],
})
export class MateriasPage implements OnInit {

  private materias = new Array();
  textoBuscar= '';

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
    this.getMaterias();
  }
  async getMaterias(){
    await this.loader.presentLoading();
    this.authService.getID().then(idUsuario =>{
    let params = {
      "idUsuario": idUsuario.toString()
    };
    let res = this.api.post("obtenerListaMaterias", params).subscribe(response => {
      this.loader.dismissLoader();
      let res = JSON.parse(response.toString());
      if(res['data'] != undefined){
        this.materias = res['data'];
      } else if(res['message'] != undefined)
        this.showError(res['message']);
     }, error => {
      this.loader.dismissLoader();
      console.log(error);
      this.showError("Error al conectarse al servidor");
    });
    });
    
  }
  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });
   await alert.present(); 
 }
 openDetail(id){
  this.getInfoMateria(id);
 }
editar(id, nombre, color){
  let navigationExtras: NavigationExtras = { state : {idMateria: id, nombre:nombre, color:color}};
      this.router.navigate(['editar-materia'], navigationExtras);
}

eliminar(id){
  let params = {
    "idMateria": id.toString()
  };
  let res = this.api.post("eliminarMateria", params).subscribe(response => {
    this.loader.dismissLoader();
    let res = JSON.parse(response.toString());
    if(res['data'] != undefined){
      this.showError(res['message']);
    } else if(res['message'] != undefined)
      this.showError(res['message']);
      this.getMaterias();
   }, error => {
    this.loader.dismissLoader();
    console.log(error);
    this.showError("Error al conectarse al servidor");
  });
}

 getInfoMateria(id){
  let params = {
    "idMateria": id.toString()
  };
  let res = this.api.post("obtenerInformacionMateria", params).subscribe(response => {
    let res = JSON.parse(response.toString());
    if(res['data'] != undefined){
      let navigationExtras: NavigationExtras = { state:{materia: JSON.stringify(res['data'])}};
      this.router.navigate(['tabs-materia'], navigationExtras);
    } else if(res['message'] != undefined){}
      this.showError(res['message']);
    }, error => {
    console.log(error);
    this.showError("Error al conectarse al servidor");
  });
}

buscar(event){
  //console.log(event);
  this.textoBuscar = event.detail.value;
}

}
