import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MateriaArchivosPageRoutingModule } from './materia-archivos-routing.module';

import { MateriaArchivosPage } from './materia-archivos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MateriaArchivosPageRoutingModule
  ],
  declarations: [MateriaArchivosPage]
})
export class MateriaArchivosPageModule {}
