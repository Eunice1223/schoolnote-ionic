import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiSchoolnoteService } from 'src/app/services/api/api-schoolnote.service';
import { LoaderService } from 'src/app/services/api/loader.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { PreviewAnyFile } from '@ionic-native/preview-any-file/ngx';
import { NavigationExtras} from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-materia-archivos',
  templateUrl: './materia-archivos.page.html',
  styleUrls: ['./materia-archivos.page.scss'],
})
export class MateriaArchivosPage implements OnInit {

  private materia;
  inAppBrowserRef;
  private hasGroup = false;
  private myFiles = new Array();
  private groupFiles = new Array();

  constructor(private api: ApiSchoolnoteService, 
    private loader: LoaderService,
    private alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute, 
    private authService: AuthService,
    private iab: InAppBrowser,
    private fileViewer: PreviewAnyFile) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.materia= JSON.parse(this.router.getCurrentNavigation().extras.state.materia);
        }
      });
     }

  ngOnInit() {
    this.hasGroup = this.materia.idGrupo != null;
    this.getFiles();
  }
  async getFiles(){
    await this.loader.presentLoading();
    this.authService.getID().then(idUsuario =>{
      let params = {
        "idUsuario": idUsuario.toString(),
        "idMateria": this.materia.idMateria.toString()
      };
      let res = this.api.post("obtenerMultimediaM", params).subscribe(response => {
        if(this.hasGroup)
          this.getGroupFiles();
        else
          this.loader.dismissLoader(); 
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.myFiles = res['data'];
          //this.myFiles.reverse();
        } else if(res['message'] != undefined){
          this.showError(res['message']);
        }
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });
  }
  getGroupFiles(){
    this.authService.getID().then(idUsuario =>{
      let params = {
        "idGrupo": this.materia.idGrupo.toString(),
        "idMateria": this.materia.idMateria.toString()
      };
      let res = this.api.post("obtenerMultimediaM", params).subscribe(response => {
        this.loader.dismissLoader();
        let res = JSON.parse(response.toString());
        if(res['data'] != undefined){
          this.groupFiles = res['data'];
        }
       }, error => {
        this.loader.dismissLoader();
        console.log(error);
        this.showError("Error al conectarse al servidor");
      });
      });
  }
  async viewFile(urlFile){
    let url = urlFile.replace(" ", "%20");
    /*await this.loader.presentLoading();
    this.fileViewer.preview(url).then((res: any) => {
      console.log(res);
      this.loader.dismissLoader();
    })
    .catch((error: any) => {
      console.log(error);
      this.loader.dismissLoader();
    });*/
    this.inAppBrowserRef = this.iab.create(url,'_blank','location=no,hardwareback=yes,toolbar=yes,enableViewportScale=yes');
    if(this.inAppBrowserRef.on('loadstop') != undefined){
      this.inAppBrowserRef.on('loadstop').subscribe(event => this.loadStopedCallBack());
      this.inAppBrowserRef.on('exit').subscribe(event => this.exitCallBack());
    } else {
      this.loader.dismissLoader();
    }
  }
  loadStopedCallBack() {
    if (this.inAppBrowserRef != undefined) {
      this.inAppBrowserRef.insertCSS({ code: "body{font-size: 25px;}" });
      this.inAppBrowserRef.show();
    }
    this.loader.dismissLoader();
}
exitCallBack() {
  this.loader.dismissLoader();
}
  async showError(message){
    const alert = await this.alertController.create({
    subHeader: message,
    buttons: ['Ok']
   });

   await alert.present(); 
 }
 openMyFiles(){
  let navigationExtras: NavigationExtras = { state:{materia: JSON.stringify(this.materia)}};
  this.router.navigate(['archivos-materia'], navigationExtras);
 }
 openGroupFiles(){
  let navigationExtras: NavigationExtras = { state:{materia: JSON.stringify(this.materia), groupFilesOnly: true}};
  this.router.navigate(['archivos-materia'], navigationExtras);
}
}
