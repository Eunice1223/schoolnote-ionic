import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MateriaArchivosPage } from './materia-archivos.page';

describe('MateriaArchivosPage', () => {
  let component: MateriaArchivosPage;
  let fixture: ComponentFixture<MateriaArchivosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MateriaArchivosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MateriaArchivosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
