import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MateriaArchivosPage } from './materia-archivos.page';

const routes: Routes = [
  {
    path: '',
    component: MateriaArchivosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MateriaArchivosPageRoutingModule {}
