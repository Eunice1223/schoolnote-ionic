import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'tabs',
    pathMatch: 'full'
  },
  {
    path: '',loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
 
  {
    path: 'registro',loadChildren: () => import('./pages/registro/registro.module').then(m => m.RegistroPageModule)
  },
  {
    path: 'perfil',
    loadChildren: () => import('./pages/perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'materias',
    loadChildren: () => import('./pages/materias/materias.module').then( m => m.MateriasPageModule)
  },
  {
    path: 'horario',
    loadChildren: () => import('./pages/horario/horario.module').then( m => m.HorarioPageModule)
  },
  {
    path: 'agregar',
    loadChildren: () => import('./pages/agregar/agregar.module').then( m => m.AgregarPageModule)
  },
  {
    path: 'unirme-grupo',
    loadChildren: () => import('./pages/unirme-grupo/unirme-grupo.module').then( m => m.UnirmeGrupoPageModule)
  },
  {
    path: 'agregar-pendiente',
    loadChildren: () => import('./pages/agregar-pendiente/agregar-pendiente.module').then( m => m.AgregarPendientePageModule)
  },
  {
    path: 'editar-pendiente',
    loadChildren: () => import('./pages/editar-pendiente/editar-pendiente.module').then( m => m.EditarPendientePageModule)
  },
  {
    path: 'agregar-foto',
    loadChildren: () => import('./pages/agregar-foto/agregar-foto.module').then( m => m.AgregarFotoPageModule)
  },
  {
    path: 'crear-grupo',
    loadChildren: () => import('./pages/crear-grupo/crear-grupo.module').then( m => m.CrearGrupoPageModule)
  },
  {
    path: 'agregar-horario',
    loadChildren: () => import('./pages/agregar-horario/agregar-horario.module').then( m => m.AgregarHorarioPageModule)
  },
  {

    path: 'sin-horario',
    loadChildren: () => import('./pages/sin-horario/sin-horario.module').then( m => m.SinHorarioPageModule)
  },
  {
    path: 'unirme-grupo',
    loadChildren: () => import('./pages/unirme-grupo/unirme-grupo.module').then( m => m.UnirmeGrupoPageModule)
  },
  {
    path: 'agregar-pendiente',
    loadChildren: () => import('./pages/agregar-pendiente/agregar-pendiente.module').then( m => m.AgregarPendientePageModule)
  },
  {
    path: 'agregar-foto',
    loadChildren: () => import('./pages/agregar-foto/agregar-foto.module').then( m => m.AgregarFotoPageModule)
  },
  {
    path: 'crear-grupo',
    loadChildren: () => import('./pages/crear-grupo/crear-grupo.module').then( m => m.CrearGrupoPageModule)
  },
  {
    path: 'archivos-materia',
    loadChildren: () => import('./pages/archivos-materia/archivos-materia.module').then( m => m.ArchivosMateriaPageModule)

  },
  {
    path: 'tabs-materia',
    loadChildren: () => import('./tabs-materia/tabs-materia.module').then(m => m.TabsMateriaPageModule)
  },
  {
    path: 'agregar-materia',
    loadChildren: () => import('./pages/agregar-materia/agregar-materia.module').then(m => m.AgregarMateriaPageModule)
  },
  {
    path: 'editar-materia',
    loadChildren: () => import('./pages/editar-materia/editar-materia.module').then( m => m.EditarMateriaPageModule)
  },
  {
    path: 'cambiar-admin',
    loadChildren: () => import('./pages/cambiar-admin/cambiar-admin.module').then( m => m.CambiarAdminPageModule)
  },
  {
    path: 'popovercomponent',
    loadChildren: () => import('./pages/popovercomponent/popovercomponent.module').then( m => m.PopovercomponentPageModule)
  },  {
    path: 'popover-etiqueta',
    loadChildren: () => import('./popover-etiqueta/popover-etiqueta.module').then( m => m.PopoverEtiquetaPageModule)
  }






]; 

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
